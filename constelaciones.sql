-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: pruebas
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `constelaciones`
--

DROP TABLE IF EXISTS `constelaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `constelaciones` (
  `idNumeros` int NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Representacion` varchar(45) DEFAULT NULL,
  `Mes mejor visibilidad` varchar(45) DEFAULT NULL,
  `Estrellas (mv <6,5)` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idNumeros`),
  UNIQUE KEY `idNumeros_UNIQUE` (`idNumeros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `constelaciones`
--

LOCK TABLES `constelaciones` WRITE;
/*!40000 ALTER TABLE `constelaciones` DISABLE KEYS */;
INSERT INTO `constelaciones` VALUES (181,'Aquarius','Sujeto con anfora','Octubre','172 '),(281,'Aries','Carnero','Diciembre','86 '),(381,'Bootes','Figura humana masculina','Junio','144 '),(481,'Cancer','Cangrejo','Marzo','104 '),(581,'Canis Mayor','Gran cazador','Febrero','147 '),(681,'Capricornus','Cabra mitad pez','Septiembre','81'),(781,'Casiopea','Trono','Octubre','157'),(981,'Crux','Cruz del Sur','Mayo','49'),(1081,'Gemini','Los mellizos','Febrero','119'),(1181,'Leo','Leon','Abril','123'),(1281,'Libra','Balanza','Junio','83'),(1381,'Orion','Cazador','Enero','204'),(1481,'Pisces','Peces','Noviembre','150'),(1581,'Sagittarius','Arquero','Agosto','194'),(1681,'Scorpius','Escorpion','Julio','167'),(1781,'Taurus','Toro','Enero','223'),(1881,'Ursa maior','Osa Mayor','Abril','209'),(1981,'Ursa minor','Osa Menor','Junio','39'),(2081,'Virgo','Figura humana femenina','Mayo','169');
/*!40000 ALTER TABLE `constelaciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-21  0:24:31
